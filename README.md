# winedt10

Shell scripts and GUI edits for Linux Wine install of WinEdt 10.3, wineHQ 6.0.
===============

**Important**: Windows MiKTeX and Windows TeXLive2021 do not install in wineHQ wine 4.0 to 6.0 as of July 2021. The scripts here apply only to a Linux TeXLive2021 install. No testing was done on a Linux MiKTeX install.

The shell scripts are written for **BASH** and Linux Mint 20.1, which has 2021 Ubuntu 20.04 LTS base.
The scripts are a start for a new install of WinEdt 10 in wineHQ wine 6.0 or later. Not all possible scripts are included here: they may be added and edited over time, especially if **issues** are posted.

Included are menu edits and image edits which add value to the install. One edit eliminates a Floating Point Exception error in the help menu which core-dumps WinEdt. The fix launches hh.exe on help file WinEdt.chm, providing the same help window with no errors. 

Other menu edits add buttons and images/icons to the menus to replace the original actions with actions appropriate for Linux. Examples: add a button to launch a Linux terminal or to launch evince on the current PDF.

TeXLive2021 Install
==============
Scripts enable the use of **Linux TeXLive2021** binaries and libraries. Install TeXLive2021 from TUG/CTAN using the Linux package. Recommended is to download the ISO, which includes everything, then mount it with **gnome-disk-image-mounter** and follow the TeXLive instructions. At least a 3GB install is required.  Binary **Latexmk** is included in TexLive2021 but possibly not installed due to the bulk of TeXLive Full (6+ GB): install it in Linux with 

`tlmgr install latexmk`

BASH Shell Scripts
==================
Essential script **fix-synctex.shell** edits each main.synctex file after it is newly created, changing a few file names to enable **SumatraPDF** to perform **forward/inverse search**. The script is used for pdflatex and pdftex scripts when there is a **--synctex switch** in the command. It is possible but not implemented in the examples here to add a button in WinEdt to run this script manually.

Essential script **winedt-pdflatex-synctex.shell** replaces Windows command pdflatex.exe in a MiKTeX install. If the command line contains **--synctex=-1**, then the script calls **fix-synctex.shell**.  A copy of the script is expected in drive c: of the WINEPREFIX (default="$HOME/.wine"). Simplicity dictates that the script contain one line specifying the wine prefix. Therefore, this script must be edited anew for each wine prefix. Simplification of the script is possible using wine Environment variable WINEPREFIX, but no details are supplied.

Essential script **winedt-pdftex-synctex.shell** replaces Windows command pdftex.exe in a MiKTeX install. If the command line contains **--synctex=-1**, then the script calls **fix-synctex.shell**.  A copy of the script is expected in the WINEPREFIX (default="$HOME/.wine"). Editing and WINEPREFIX remarks parallel the pdflatex script.

Essential script **winedt-latexmk.shell** replaces Windows command latexmk.exe in a MiKTeX install. A copy of the script is expected in the WINEPREFIX (default="$HOME/.wine"). Editing and WINEPREFIX remarks parallel the pdflatex script. Special to latexmk is the local setup file latexmkrc or .latexmkrc, which is used if it exists. A possible use switches PDF viewer when using latexmk. An example: The 1-line contents of $HOME/.latexmkrc:

`$pdf_previewer = "start /usr/bin/evince %O %S";`

Utility script **$HOME/bin/show-args.shell** is used to DEBUG the scripts. Its purpose is to display shell script echo results, in script order, waiting for an escape key to continue. With no arguments, it simply waits for an escape key.

Other shell scripts omitted: Tex, LaTeX, AMStex and others in WinEdt's **Options** ==> **Execution Modes** main menu items. Consider the provided shell scripts as templates to enable missing features.

Test Files
==========
Pdflatex and pdftex can be tested using files from CTAN:  

PDFTeX:   https://ctan.org/pkg/gentle?lang=en

PDFLaTeX: https://ctan.org/search?phrase=texbytopic

New Buttons
=============
The WinEdt Options Interface allows new buttons to be added. It is not a one-step task. The first step is to add the button into **Toolbar.ini**. The second step is to add an action for the button in **Mainmenu.ini**. These entries must match case and syntax of existing buttons. **Important**: edit the files in WinEdt's *Options Interface** and then click the button in the **Options Interface** to enable the edits.

A detailed example to add a button for **Wordpad** appears below in section **Editing the Legacy Toolbar: An Example**.

If adding an icon or image, then start with an icon that already exists on a button. There will be duplicate icons on buttons temporarily. Then change the icon later after dealing with folder **myImages** in the USER directory (next topic below).

Images and Icons
================== 
**Images and icons** used in WinEdt menus are expected to be 16x16 transparent. Conversion of any .png can be made by GIMP. The directory **myImages** included here contains some examples. In WinEdt 10, directory **myImages** is to be copied to the USER XXXXX directory (create missing directories):

`c:/users/XXXXX/Application Data/WinEdt Team/WinEdt 10/Bitmaps/myImages`

That is not the end of it. Within WinEdt, the **Options Interface** must be used to edit **Images.ini** then enable the new images (read the documentation in **Images.ini**), which means: click a button in the Options Interface. Didn't work? Restart WinEdt, repeat the steps.

Edits to Toolbar.ini and MainMenu.ini
==============
The edits are applied in the **Options Interface** in WinEdt. To enable an edit, click the button far left second row of the Options Interface. Edits are saved in

`c:/users/XXXXX/Application Data/WinEdt Team/WinEdt 10/ConfigEx/Toolbar.ini`

`c:/users/XXXXX/Application Data/WinEdt Team/WinEdt 10/ConfigEx/MainMenu.ini`

Common mistake: editing with xed/gedit in Program Files!

**Enable Legacy Toolbar**
<details><summary>Click to expand Legacy toolbar details</summary>

WinEdt 10 has a menu item in **Options** to choose the toolbar style from 8 styles.
The effect is to edit Toolbar.ini, changing comment markers. You can do it yourself
by editing Toolbar.ini in the **Options Interface**.

Choosing the toolbar does not edit the contents. That must be done in the **Options
Interface**.

To change manually to the Legacy Toolbar, all %%INCLUDE lines in Toolbar.ini must be commented out, then lines after line 136 (2-row Legacy Toolbar) can be edited to customize the buttons and drop-down menus. 

**Important**:  Save the file Toolbar.ini. To enable the changes, double-click on the icon far left on row 2 of the **Options Interface**. 
</details>

**Editing the Legacy Toolbar: An Example**
<details><summary>Click to expand example details</summary>
Assumed: Legacy toolbar enabled. The example adds at the end of file ToolBar.ini this button:

    BUTTON="Wordpad"   // Location: Toolbar.ini

**Important**: The inserted BUTTON (or MENU) must exist as an ITEM in MainMenu.ini. For instance:

    ITEM="Wordpad"   // Location: MainMenu.ini, see the 4 lines below

**Editing MainMenu.ini**

Example continued. In the **Options Interface** double-click on MainMenu.ini. This opens an editor window with the same file name, but destination in the User folder. The example inserts into **MainMenu.ini** about line 3429:

    ITEM="Wordpad"
    CAPTION="Wordpad"
    IMAGE="Asymptote" // Later, use wordpad.png 16x16 transparent
    MACRO="Run('c:\Program Files\Windows NT\Accessories\wordpad.exe','%!P',0,0,'Wordpad');"

The image **Asymptote.png** already exists in WinEdt. To use image **wordpad.png** instead requires learning how to add images to WinEdt, another project. See **Images and Icons** above.

**Important**:  Save the file MainMenu.ini. To enable the changes, double-click on the icon far left on row 2 of the **Options Interface**. 

</details>

Terminal and Evince Enhancements
==================

To add buttons and actions for Mate-Terminal and Evince requires similar effort as illustrated above.

<details><summary>
Click to expand details for EVINCE and Mate-Terminal in MainMenuMenu.ini
</summary>
<code>
    ITEM="PDF_Search" // about line 3580
      CAPTION="PDF &Search"
      IMAGE="SumatraPDFsearch"
      MACRO="Exe('%b\Exec\PDF\PDF Search.edt');"
      SHORTCUT="8311::Shift+F8"
      REQ_FILTER="%P\%N.pdf*%P\%N%$(!'PDF-SyncEx');"
      REQ_DOCUMENT=1
      REQ_FILE=1
    ITEM="EvincePDFviewer"
      CAPTION="Evince PDF Viewer on MAIN pdf"
      IMAGE="evince"
      MACRO=`DosToUnix('%P\%N.pdf',0);
             SubstituteInString('%!0','H:/','/home/grant/',0,0);
             Run('/usr/bin/evince "%!0"','%!P',0,0,'evince',1,1);`
      REQ_FILTER="%P\%N.pdf"
    ITEM="-"
    // 
    ITEM="Command_Prompt" // about line 3409
      CAPTION="windows command prompt"
      IMAGE="CommandPrompt"
      MACRO="Run('%$(|CMD|);','%!P',0,0,'%$(|CMDTitle|);',1,1);"
    ITEM="MateTerminal"
      CAPTION="mate-terminal"
      IMAGE="terminal" // must be in folder myImages - see above
      MACRO="Run('/usr/bin/mate-terminal','%!P',0,0,'mate-terminal',1,1);"
</code>
</details>

Help Menu Edits
==============

The main help is in **WinEdt.chm**. Unfortunately, WinEdt crashes with a Floating point exception. To eliminate this behavior, launch WinEdt.chm using Wine's hh.exe. The fix is permanent. The other issues in the WinEdt **Help Menu** are fixed by the following edits, in which comment mark // has preserved the original text. Some items were eliminated because they provided duplicate information.
The edits are applied to **MainMenu.ini** from the **Options Interface**.

<details><summary>Click to expand the Help Menu edits in MainMenu.ini</summary>
<code>
MENU="&Help"
  CAPTION="&Help"
  ITEM="WinEdt_Manual"
    CAPTION="&WinEdt Manual"
    IMAGE="Help"
    //MACRO="HTMLHelp('%B\Doc\Help\WinEdt.chm',1,1);"
    MACRO=`Run('c:\windows\hh.exe %B\Doc\Help\WinEdt.chm', '%!P');`
    SHORTCUT="112::F1"
    // Removed keyword search, due to fatal error
 // ITEM="-"
 // ITEM="Keyword_Search"
   // CAPTION="&Keyword Search"
   // IMAGE="KeywordSearch"
   // MACRO="Exe('%b\Menus\Help\Keyword Help.edt');"
   // SHORTCUT="16496::Ctrl+F1"
  ITEM="-"
  ITEM="LaTeX_Doc"
    CAPTION="LaTeX &Doc..."
    CONFIG_FILTER="Default;MiKTeX"
    IMAGE="DocLaTeX"
    MACRO="Exe('%b\Macros\Doc\TeXDoc.edt');"
    MACRO=`Run(|/usr/bin/firefox https://www.ctan.org/search|);`
    SHORTCUT="24688::Shift+Ctrl+F1"
  ITEM="TeX_Doc"
    CAPTION="TeX &Doc..."
    //CONFIG_FILTER="TeX Live"
    IMAGE="DocLaTeX"
    MACRO="Run('/usr/local/bin/texdoctk');"
    // TeX Live 2012 ÷ 2009, < 2008
   // MACRO="WinExe('','texdoctk.exe','','',1000);"
    SHORTCUT="24688::Shift+Ctrl+F1"
   // REQ_FILTER="%$('TeX-Bin');\texdoctk.exe"
    // TeX Live 2008
    // MACRO="WinExe('','texdoctk.bat','','',1000);"
    // REQ_FILTER="%$('TeX-Bin');\texdoctk.bat"
  ITEM="LaTeX_Help_e-Book"
    CAPTION="&LaTeX Help e-Book"
    CONFIG_FILTER="Default;MiKTeX;TeX Live"
    IMAGE="BookLaTeX"
    //MACRO="HTMLHelp('%B\Doc\LaTeXHelpBook\LatexHelpBook.chm',1,1);"
     MACRO=`Run('c:\windows\hh.exe %B\Doc\LaTeXHelpBook\LatexHelpBook.chm', '%!P');`
  ITEM="MiKTeX_Help"
    CAPTION="&MiKTeX"
    CONFIG_FILTER="MiKTeX"
    IMAGE="TeXMiKTeX"
    //MACRO="HTMLHelp(|%$('TeX-Root');\doc\miktex\miktex.chm|,1,1);"
    MACRO=`Run(|c:\windows\hh.exe %$('TeX-Root');\doc\miktex\miktex.chm|, '%!P');`
    REQ_FILTER="%$('TeX-Root');\doc\miktex\miktex.chm"
  ITEM="TeX_Live_Guide"
    CAPTION="&TeX Live Guide"
    //CONFIG_FILTER="TeX Live"
    IMAGE="TeXLive"
    // TeX Live 2012 ÷ 2009
    MACRO=`ShellExecute("open","%$('TeX-Help');\index.html","",`+
          `"%$('TeX-Help');",1);`
    REQ_FILTER="%$('TeX-Help');\index.html"
    // TeX Live 2008
    // MACRO=`Run(|%$('PDF-View'); `+
    //       `"%$('TeX-Root');\texmf-doc\doc\english\texlive-en\texlive-en.pdf"|);`
    // REQ_FILTER="%$('TeX-Root');\texmf-doc\doc\english\texlive-en\texlive-en.pdf"
    // TeX Live < 2008
    // MACRO=`Run(|%$('PDF-View'); `+
    //       `"%$('TeX-Root');\texmf-doc\doc\english\texlive-en\live.pdf"|);`
    // REQ_FILTER="%$('TeX-Root');\texmf-doc\doc\english\texlive-en\live.pdf"
  ITEM="Browse_TeX_Doc"
    CAPTION="&Browse TeX Doc..."
    CONFIG_FILTER="Default;MiKTeX;TeX Live"
    IMAGE="Browse"
    // MiKTeX; TeX Live 2012 ÷ 2009
    MACRO=`Run(|explorer.exe /n,/e, "%$('TeX-Help');"|,|%$('TeX-Help');|,0,0,`+
          `'Windows Explorer');`
    REQ_FILTER="%$('TeX-Help');\"
    // TeX Live < 2009
    // MACRO=`Run(|explorer.exe /n,/e, "%$('TeX-Root');\texmf-doc\doc"|,`+
    //       `|%$('TeX-Root');\texmf-doc\doc|,0,0,'Windows Explorer');`
    // REQ_FILTER="%$('TeX-Root');\texmf-doc\doc\"
  ITEM="-"
  ITEM="HTML_Tag_Reference"    
</code>
</details>




